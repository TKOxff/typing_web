import React from "react";
import Article from "./Article";

//
// https://www.youtube.com/watch?v=j4t0aZge0Mc&t=408s
// 日本一わかりやすいReact入門#5: コンポーネントのstateの設定・取得・変更
//
class Blog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPublished: false,
      order: 1,
    };
  }

  togglePublished = () => {
    this.setState({
      isPublished: !this.state.isPublished,
    });
  };

  render() {
    return (
      <div className="App">
        <Article
          title={"React State Test"}
          isPublished={this.state.isPublished}
          toggle={() => this.togglePublished()}
        />
      </div>
    );
  }
}

export default Blog;
