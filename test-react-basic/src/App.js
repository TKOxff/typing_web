import './css/App.css';
import Blog from "./Blog"

//
// 日本一わかりやすいReact入門
//
function App() {
  return (
    <div className="App">
      <Blog />
    </div>
  );
}

export default App;
