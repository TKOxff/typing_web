import React from "react";

const Article = (props) => {
  return (
    <div className="App">
      <h1>{props.title}</h1>
      <label htmlFor="check">Public State</label>
      <input
        type="checkbox"
        checked={props.isPublished}
        id="check"
        onClick={props.toggle}
      />
    </div>
  );
};

export default Article;
