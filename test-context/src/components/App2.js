import React from 'react';
import '../css/App.css';

const ThemeConext = React.createContext('light');

//
// https://reactjs.org/docs/context.html
// 
function App() {
  return (
    <ThemeConext.Provider value="dark">
      <Toolbar />
    </ThemeConext.Provider>
  );
}


function Toolbar(props) {
  return (
    <ThemedButton />
  )
}

class ThemedButton extends React.Component {
  static contextType = ThemeConext;

  render() {
    return <Button theme={this.context} />
  }
}

export default App;
