import React from 'react';
import '../css/App.css';

function App() {
  return (
    <Toolbar theme="dark" />
  );
}


function Toolbar(props) {
  return (
    <ThemedButton theme={props.theme} />
  )
}

class ThemedButton extends React.Component {
  render() {
    return <Button theme={this.props.theme} />
  }
}

export default App;
