import React from "react";
import { useState, useEffect } from "react";
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import "bootstrap/dist/css/bootstrap.css";
import * as legoData from "./notties/loading-cat.json";
import * as doneData from "./notties/done-tick.json";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: legoData.default,
  // rendererSettings: {
  //   preserveAspectRatio: "xMidYMid slice",
  // },
};

const defaultOptions2 = {
  loop: false,
  autoplay: true,
  animationData: doneData.default,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

function Loading() {
  const [isDone, setDone] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => {
      console.log("timeer 1 sec");
      setDone(true);
    }, 1000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <FadeIn>
      <div className="d-flex justify-content-center align-items-center">
        <h1>Loading...</h1>
        {isDone === false ? (
          <Lottie options={defaultOptions} height={120} width={120} />
        ) : (
          <Lottie options={defaultOptions2} height={120} width={120} />
        )}
      </div>
    </FadeIn>
  );
}

export default Loading;
