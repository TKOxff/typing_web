// import logo from './logo.svg';
import './App.css';
// import Loading from "./loading.js"
import Loading from "./loading_fn.js"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Loading />
      </header>
    </div>
  );
}

export default App;
