import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "react-tiger-transition/styles/main.min.css";
import { Navigation, Route, Link, cube } from "react-tiger-transition";
import "./App.css";

// inject glide styles
cube({
  name: "cube-left",
});
cube({
  name: "cube-right",
  direction: "right",
});

function App() {
  return (
    <Router>
      <Navigation>
        <Route exact path="/about">
          <About />
        </Route>
        <Route exact path="/users">
          <Users />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
      </Navigation>
    </Router>
  );
}

export default App;

function Home() {
  return (
    <div style={{ background: "#6ddccf" }}>
      <h2>Home</h2>
      <ul>
        <li>
          <Link to="/about" transition="cube-left">
            About
          </Link>
        </li>
        <li>
          <Link to="/users" transition="cube-left">
            Users
          </Link>
        </li>
      </ul>
    </div>
  );
}

function About() {
  return (
    <div style={{ background: "#f9ed69" }}>
      <h2>About</h2>
      <Link to="/" transition="cube-right">
        back to Home
      </Link>
    </div>
  );
}

function Users() {
  return (
    <div style={{ background: "#a8d8ea" }}>
      <h2>Users</h2>
      <Link to="/" transition="cube-right">
        back to Home
      </Link>
    </div>
  );
}
