import React from "react";
import { Link, useParams } from "react-router-dom";
import "./css/Typing.css";
import TypingFactory from "./TypingFactory";

//
// https://ko.reactjs.org/docs/composition-vs-inheritance.html
// 공식 사이트에서
// 항상 함수 컴포넌트의 합성(Composiition)을 사용하고
// 클래스 상속(Inheritance)는 쓰지 말라고, 쓸 곳이 없다고 한다...
//
function Typing(props) {
  let { type } = useParams();

  function baseFunc() {
    return "this is base func result"
  }
  
  return (
    <div className="Typing">
      <h2>Typing Base</h2>
      <Link to="/">goto AppHome</Link>

      <div>
        <TypingFactory type={type} commonFunc={baseFunc} />
      </div>
    </div>
  );
}

export default Typing;