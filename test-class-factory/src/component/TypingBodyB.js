import React from "react";
import "./css/Typing.css";

export default function TypingChildB() {
  return (
    <div className="TypingB">
      <h2>Title ChildBB</h2>
      
      <div>
        <h2>Body-Child.BB</h2>
      </div>
    </div>
  );
}
