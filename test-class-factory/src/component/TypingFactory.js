import "./css/Typing.css";
import TypingBodyA from "./TypingBodyA";
import TypingBodyB from "./TypingBodyB";

function TypingFactory(props) {
  switch (props.type) {
    case "A":
      return <TypingBodyA commonFunc={props.commonFunc} />
    case "B":
      return <TypingBodyB commonFunc={props.commonFunc} />
    default:
      return <div>Not Found Type:{props.type}</div>
  }
}

export default TypingFactory;