import React from "react";
import "./css/Typing.css";

export default function TypingBodyA(props) {
  return (
    <div className="Typing">
      <h2>Title ChildA</h2>

      <div>
        <h2>Body-Child.A</h2>
        <h3>{props.commonFunc()}</h3>
      </div>
    </div>
  );
}
