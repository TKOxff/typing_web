import "./css/App.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Typing from "./Typing";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/typing/:type">
            <Typing />
          </Route>
          <Route path="/">
            <AppHome />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Links() {
  return (
    <ul>
      <li>
        <Link to="/typing/A">Typing-A</Link>
      </li>
      <li>
        <Link to="/typing/B">Typing-B</Link>
      </li>
      <li>
        <Link to="/typing/C">Typing-C</Link>
      </li>
    </ul>
  );
}

function AppHome() {
  return (
    <div>
      <h2>AppHome</h2>
      <Links />
    </div>
  );
}

export default App;
