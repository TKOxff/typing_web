const lessonData = [
  {
    course_title: "Basic",
    course_array: [
      {
        title: "ひらがな",
        letter_count: 1,
        type: "letter",
        lines: [
          "あいうえお",
          "かきくけこ",
          "さしすせそ",
          "たちつてと",
          "なにぬねの",
          "はひふへほ",
          "まみむめも",
          "やゆよ",
          "らりるれろ",
          "わをん",
        ],
      },
      {
        title: "だくおん",
        letter_count: 1,
        type: "letter",
        lines: [
          "がぎぐげご",
          "ざじずぜぞ",
          "だぢづでど",
          "ばびぶべぼ",
          "ぱぴぷぺぽ",
        ],
      },
      {
        title: "ようおん", // CONTRACTIONS [YA YI YU YE YO]",
        letter_count: 2,
        type: "letter",
        lines: [
          "きゃきゅきょ",
          "しゃしゅしょ",
          "ちゃちゅちょ",
          "にゃにゅにょ",
          "ひゃひゅひょ",
          "みゃみゅみょ",
          "りゃりゅりょ",
        ],
      },
      {
        title: "ようおん だくおん", //（濁音）CONTRACTIONS [YA YI YU YE YO]",
        letter_count: 2,
        type: "letter",
        lines: [
          "ぎゃぎゅぎょ",
          "じゃじゅじょ",
          "ぢゃぢゅぢょ",
          "びゃびゅびょ",
          "ぴゃぴゅぴょ",
        ],
      },
      {
        title: "ようおん だくおん 2", //CONTRACTIONS V, F (katakana F7)",
        letter_count: 2,
        type: "letter",
        lines: ["ヴァヴィヴェヴォ", "ファフィフェフォ"],
      },
      {
        title: "小さい文字", // litte - l",
        letter_count: 1,
        type: "letter",
        lines: ["ぁぃぅぇぉ"],
      },
    ], // course_array
  },
  //--course-------------------------------------------------------------------
  {
    course_title: "日本語入力方法",
    course_array: [
      {
        title: "日本語入力方法 - ひらがな入力",
        letter_count: 1,
        type: "tutorial",
        lines: [
          "あい",
          "あ　い　う　え　お",
          "ああ　いい　うう　ええ　おお",
          "あああ　いいい",
          "ううう　えええ",
          "おおお",
          "あおい",
        ],
        tips: [
          "기본이 되는 히라가나 입력입니다.\n각 히라가나에 대응하나 로마자를 입력합니다.",
          "",
        ]
      },
    ], // course_array
  },
  //--course-------------------------------------------------------------------
  {
    course_title: "小学 1 年生の漢字ドリル",
    course_array: [
      {
        title: "小学 1年生-1",
        letter_count: 1,
        type: "sentence",
        lines: [
          "先月(せん げつ)はなつ休(やす)みだった。",
          "山(やま)の中(なか)にある村(むら)。",
          "大(おお)きな男(おとこ)の先生(せん せい)。",
          "百円玉(ひゃく えん だま)が五枚(ご まい)。",
          "四角(し かく)い石(いし)を見(み)つけた。",
          "花(はな)の下(した)に虫(むし)がいた。",
          "大中小(だい ちゅう しょう)のじゅんばん。",
          "王女(おう じょ)さまと王子(おう じ)さま。",
        ],
      },
      {
        title: "小学 1年生-2",
        letter_count: 1,
        type: "sentence",
        lines: [
          "五百円(ご ひゃく えん)の本(ほん)をかう",
          "村(むら)の小川(お がわ)にさく花(はな)",
          "四月(し がつ)から小学生(しょう がく せい)",
          "夕(ゆう)やけの空(そら)を見(み)る",
          "気(き)に入(い)っている本(ほん)",
          "林(りん)の中(なか)に大木(たい ぼく)がある",
          "こん虫(むし)の足(あし)は六本(ろく ほん)",
          "おいしい目玉(め だま)やき",
        ],
      },
      {
        title: "小学 1年生-3",
        letter_count: 1,
        type: "sentence",
        lines: [
          "手(て)と足(あし)が大(おお)きい男(おとこ)",
          "川(がわ)の水(みず)が白(しろ)くにごる",
          "上中下(じょう ちゅう げ)のじゅんばん",
          "青空(あお ぞら)を見上(み あ)げた",
          "三日(さん にち)は誕生日(たん じょう び)",
          "森林(しん りん)を大切(たい せつ)にする",
          "十一月(じゅう いち がつ)三日(さん にち)は文化(ぶん か)の日(ひ)",
          "小学校(しょう がっ こう)の出入(で い)り口(ぐち)",
        ],
      },
      {
        title: "小学 1年生-4",
        letter_count: 1,
        type: "sentence",
        lines: [
          "四月(し がつ)で二年生(に ねん せい)になる。",
          "田(た)んぼの土(つち)をほる。",
          "大(おお)きな花火(はな び)の音(おと)。",
          "九月(く がつ)四日(よっ か)はえん足(あし)がある。",
          "三時(さん じ)に一休(ひと やす)みする。",
          "正(ただ)しい本(ほん)の名前(な まえ)をいう。",
          "竹(たけ)の先(さき)に糸(いと)をつける。",
          "山(やま)の上(うえ)にある村(むら)にいく。",
        ],
      },
      {
        title: "小学 1年生-5",
        letter_count: 1,
        type: "sentence",
        lines: [
          "空(そら)に三日月(み か づき)を見(み)つけた。",
          "小(ちい)さな村(むら)の人口(じん こう)をしる。",
          "天(てん)気(き)は雨(あめ)でした。",
          "大空(おお ぞら)を見上(み あ)げる。",
          "草花(くさ ばな)にくわしい男(おとこ)の子(こ)。",
          "出入(で い)り口(ぐち)は左(ひだり)。",
          "正(ただ)しい文字(も じ)を学(まな)ぶ。",
          "七五三(しち ご さん)をいわう。",
        ],
      },
      {
        title: "小学 1年生-6",
        letter_count: 1,
        type: "sentence",
        lines: [
          "田(た)んぼにいる小(ちい)さい虫(むし)。",
          "赤(あか)い金魚(きん ぎょ)をかう。",
          "先生(せん せい)の気持(き も)ちをしる。",
          "子犬(こ いぬ)が五(ご)ひき生(う)まれた。",
          "五月(ご がつ)五日(いつ か)は子(こ)どもの日(ひ)。",
          "大(おお)きな町(まち)の小学校(しょう がっ こう)。",
          "森(もり)でまよった子犬(こい ぬ)。",
          "大木(たい ぼく)の下(した)にさく花(はな)。",
        ],
      },
      {
        title: "小学 1年生-7",
        letter_count: 1,
        type: "sentence",
        lines: [
          "上下(じょう げ)左右(さ ゆう)にうごく。",
          "天女(てん にょ)のでんせつ。",
          "水中(すい ちゅう)で貝(かい)を見(み)つけた。",
          "生卵(なま たまご)をたべた。",
          "上(のぼ)りと下(くだ)りの電車(でん しゃ)。",
          "青(あお)い糸(いと)でぬう。",
          "十人(じゅう にん)の名前(な まえ)をおぼえる。",
          "七日(なな にち)と八日(よう か)。",
        ],
      },
      {
        title: "小学 1年生-8",
        letter_count: 1,
        type: "sentence",
        lines: [
          "正(ただ)しい文字(も じ)をかく。",
          "火山(か ざん)にちかい町(まち)。",
          "五日(ご にち)は大雨(おお あめ)のよほう。",
          "千円(せん えん)さつを出(だ)す。",
          "川(かわ)の水(みず)が田(た)んぼに入(はい)る。",
          "雨天中止(う てん ちゅう し)です。",
          "赤(あか)ちゃんが立(た)ち上(あ)がる。",
          "漢字(かん じ)のテストは九十点(きゅう じっ てん)。",
        ],
      },
      {
        title: "小学 1年生-9",
        letter_count: 1,
        type: "sentence",
        lines: [
          "赤(あか)ちゃんの手(て)は小(ちい)さい。",
          "左足(ひだり あし)で石(いし)をける。",
          "早口(はや くち)ことばがすきな人(ひと)。",
          "糸車(いと ぐるま)をまわす。",
          "村(むら)で一番(いち ばん)の力(ちから)もち。",
          "耳(みみ)を立(た)てるウサギさん。",
          "森(もり)で見(み)つけた小鳥(こ とり)。",
          "来月(らい げつ)の二日(ふ つか)はお休(やす)みです。",
        ],
      },
    ], // course_array
  },
  //--course-------------------------------------------------------------------
  {
    course_title: "小学 2 年生の漢字ドリル",
    course_array: [
      {
        title: "小学 2 年生-1",
        letter_count: 1,
        type: "sentence",
        lines: [
          "図書室(と しょ しつ)に行(い)く。",
          "答(こた)えを記入(き にゅう)する。",
          "金曜日(きん よう び)に出(で)かける。",
          "父親(ちち おや)におこられた。",
          "教(おし)え方(かた)を考(かんが)える。",
          "テスト用紙(よう し)。",
          "はりに糸(いと)を通(とお)す。",
          "朝食(ちょう しょく)を作(つく)る母(はは)。",
        ],
      },
    ], // course_array
  },
  //--course-------------------------------------------------------------------
  {
    course_title: "Slack",
    course_array: [
      {
        title: "会社言葉",
        letter_count: 1,
        type: "sentence",
        lines: [
          "全未読(ぜん み どく)メッセージ確認(かく にん)終了(しゅう りょう)",
          "ちょっと休憩(きゅう けい)して、疲(つか)れた脳(のう)に糖分(とう ぶん)補給(ほ きゅう)",
          "本日(ほん じつ)の業務(ぎょう む)は終了(しゅう りょう)にします。",
          "お疲(つか)れ様(さま)でした。",
          "お昼(ひる)に行(い)ってきます。",
          "出社(しゅっ しゃ)・退社(たい しゃ)打刻(だ こく)がされておりません。",
          "これより在宅(ざい たく)勤務(きん む)を開始(かい し)します。",
          "確認(かく にん)いただきありがとうございます。",
          "よろしければお越(こ)しください。",
        ],
      },
      {
        title: "メルカリ",
        letter_count: 1,
        type: "sentence",
        lines: [
          "コメント失礼(しつ れい)いたします。",
          "早々(そう そう)にご対応(たい おう)いただきありがとうございます。",
          "到着(とう ちゃく)を楽(たの)しみに待(ま)っております。",
        ],
      },
    ], // course_array
  },
];

const invalidLessonData = {
  course_title: "invalid",
  course_array: [
    {
      title: "invalid",
      letter_count: 0,
      type: "",
      lines: [],
    },
  ], // course_array
};

// export { lessonSentences as Lessons };
export default lessonData;

export function getLesson(lv, course_id) {
  if (course_id >= lessonData.length) {
    return invalidLessonData;
  } else if (lv >= lessonData[course_id].course_array.length) {
    return invalidLessonData;
  }

  // console.log("getLesson lv:" + lv + " course_id:" + course_id);
  return lessonData[course_id].course_array[lv];
}

export function getLessonMaxLv(course_id) {
  return lessonData[course_id].length - 1;
}

export const isAlphabet = function (str) {
  return /^[a-zA-Z]$/.test(str);
};

export const isHiragana = function (ch) {
  ch = ch[0];
  return ch >= "\u3040" && ch <= "\u309f";
};

export const isKatakana = function (ch) {
  ch = ch[0];
  return ch >= "\u30a0" && ch <= "\u30ff";
};

export const isKana = function (ch) {
  return isHiragana(ch) || isKatakana(ch);
};

export const isKanji = function (ch) {
  ch = ch[0];
  return (
    (ch >= "\u4e00" && ch <= "\u9fcf") ||
    (ch >= "\uf900" && ch <= "\ufaff") ||
    (ch >= "\u3400" && ch <= "\u4dbf")
  );
};

export const isJapanese = function (ch) {
  return isKana(ch) || isKanji(ch);
};

const hiraganaToRomajiMap = {
  あ: "a",
  い: "i",
  う: "u",
  え: "e",
  お: "o",
  か: "ka",
  き: "ki",
  く: "ku",
  け: "ke",
  こ: "ko",
  さ: "sa",
  し: "si",
  す: "su",
  せ: "se",
  そ: "so",
  た: "ta",
  ち: "ti",
  つ: "tu",
  て: "te",
  と: "to",
  な: "na",
  に: "ni",
  ぬ: "nu",
  ね: "ne",
  の: "no",
  は: "ha",
  ひ: "hi",
  ふ: "hu",
  へ: "he",
  ほ: "ho",
  ま: "ma",
  み: "mi",
  む: "mu",
  め: "me",
  も: "mo",
  ら: "ra",
  り: "ri",
  る: "ru",
  れ: "re",
  ろ: "ro",
  や: "ya",
  ゆ: "yu",
  よ: "yo",
  わ: "wa",
  を: "wo",
  ん: "nn",

  が: "ga",
  ぎ: "gi",
  ぐ: "gu",
  げ: "ge",
  ご: "go",
  ざ: "za",
  じ: "zi",
  ず: "zu",
  ぜ: "ze",
  ぞ: "zo",
  だ: "da",
  ぢ: "di",
  づ: "du",
  で: "de",
  ど: "do",
  ば: "ba",
  び: "bi",
  ぶ: "bu",
  べ: "be",
  ぼ: "bo",
  ぱ: "pa",
  ぴ: "pi",
  ぷ: "pu",
  ぺ: "pe",
  ぽ: "po",

  きゃ: "kya",
  きぃ: "kyi",
  きゅ: "kyu",
  きぇ: "kye",
  きょ: "kyo",
  しゃ: "sya",
  しぃ: "syi",
  しゅ: "syu",
  しぇ: "sye",
  しょ: "syo",
  ちゃ: "tya",
  ちぃ: "tyi",
  ちゅ: "tyu",
  ちぇ: "tye",
  ちょ: "tyo",
  にゃ: "nya",
  にぃ: "nyi",
  にゅ: "nyu",
  にぇ: "nye",
  にょ: "nyo",
  ひゃ: "hya",
  ひぃ: "hyi",
  ひゅ: "hyu",
  ひぇ: "hye",
  ひょ: "hyo",
  みゃ: "mya",
  みぃ: "myi",
  みゅ: "myu",
  みぇ: "mye",
  みょ: "myo",
  りゃ: "rya",
  りぃ: "ryi",
  りゅ: "ryu",
  りぇ: "rye",
  りょ: "ryo",

  ぎゃ: "gya",
  ぎぃ: "gyi",
  ぎゅ: "gyu",
  ぎぇ: "gye",
  ぎょ: "gyo",

  じゃ: "zya",
  じぃ: "zyi",
  じゅ: "zyu",
  じぇ: "zye",
  じょ: "zyo",

  ぢゃ: "dya",
  ぢぃ: "dyi",
  ぢゅ: "dyu",
  ぢぇ: "dye",
  ぢょ: "dyo",

  びゃ: "bya",
  びぃ: "byi",
  びゅ: "byu",
  びぇ: "bye",
  びょ: "byo",

  ぴゃ: "pya",
  ぴぃ: "pyi",
  ぴゅ: "pyu",
  ぴぇ: "pye",
  ぴょ: "pyo",

  ヴァ: "va",
  ヴィ: "vi",
  ヴ: "vu", // 예외처리
  ヴェ: "ve",
  ヴォ: "vo",

  ファ: "fa",
  フィ: "fi",
  フェ: "fe",
  フォ: "fo",

  ぁ: "la",
  ぃ: "li",
  ぅ: "lu",
  ぇ: "le",
  ぉ: "lo",

  っ: "tt", // 예외처리
};

export function toRomaji(str) {
  return hiraganaToRomajiMap[str];
}

// export default sentences;
