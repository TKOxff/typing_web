import React, { useState } from "react";
//local
import "../css/App.css";
import TypingNormal from "./TypingNormal";
import TypingSentence from "./TypingSentence";
import TypingTutor from "./TypingTutor";

//
// 팩토리 패턴 컴포넌트
//  - 기본 적인 동적인 분할은 동작
//  - 상위 컴포넌트의 함수를 하위에 전달 가능 (생각보단 잘 동작)
//  * 문제는 하위 컴포넌트들에 동일한 props를 전달해야 하는 코드 중복.. 
//
export default function TypingFactory(props) {

  console.log(props);

  switch (props.factoryId) {
    case "tutorial":
      return (
        <TypingTutor
          lesson={props.lesson}
          onNextLevel={props.onNextLevel}
          lineStep={props.lineStep}
          onLineClear={props.onLineClear}

          line={props.line}
          typing={props.typing}
          doKeyUp={props.doKeyUp}
          currentRomaji={props.currentRomaji}
        />
      );
    case "sentence":
      return (
        <TypingSentence
          lesson={props.lesson}
          onNextLevel={props.onNextLevel}
          lineStep={props.lineStep}
          onLineClear={props.onLineClear}

          line={props.line}
          typing={props.typing}
          doKeyUp={props.doKeyUp}
          lpm={props.lpm}
          accuracy={props.accuracy}
          updateAccuracy={props.updateAccuracy}

          currentRomaji={props.currentRomaji}

          words={props.words}
        />
      );
    default:
      return (
        <TypingNormal
          lesson={props.lesson}
          onNextLevel={props.onNextLevel}
          lineStep={props.lineStep}
          onLineClear={props.onLineClear}

          line={props.line}
          typing={props.typing}
          doKeyUp={props.doKeyUp}
          lpm={props.lpm}
          accuracy={props.accuracy}
          updateAccuracy={props.updateAccuracy}

          currentRomaji={props.currentRomaji}
        />
      );
  }
}
