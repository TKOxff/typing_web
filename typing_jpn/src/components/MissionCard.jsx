import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link, Route, Switch, useRouteMatch } from "react-router-dom";
import { Avatar } from "@material-ui/core";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { red } from "@material-ui/core/colors";
// local
// import TypingMain from "./TypingMain";

const useStyles = makeStyles({
  root: {
    height: 180,
    width: 240,
    minWidth: 100,
    // maxWidth: 275,
    // border: 0,
    // borderRadius: 5,
    boxShadow: "0 -1px 5px rgba(0,0,0,0.12), 0 3px 2px rgba(0,0,0,0.24)",
    "&:hover, &:focus": {
      boxShadow: "0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)",
    },
    // padding: '15px',
    margin: "10px",
  },
  title: {
    fontSize: 20,
  },
  pos: {
    marginBottom: 12,
  },
  avatar: {
    // backgroundColor: "#ffc75f",
    backgroundColor: "light-gray",
  },
});

export default function MissionCard(props) {
  const thisStyle = useStyles();

  function doCardClick() {
    console.log("card click");
  }

  return (
    <Card className={thisStyle.root}>
      <Link
        to={`/main/${props.id}/course/${props.courseNo}`}
        style={{ color: "inherit", textDecoration: "inherit" }}
      >
        <CardHeader
          avatar={<Avatar className={thisStyle.avatar}>{props.id}</Avatar>}
          // avatar={props.id}
          // action={<CheckCircleIcon />}
          title={props.title}
        />

        <CardContent style={{ textAlign: "center" }}>
          <Typography variant="h6">
            {/* {props.title} */}
            {props.subtitle}
          </Typography>
          {/* <Typography variant="body2" component="p">
              {props.subtitle}
            </Typography> */}
        </CardContent>

        <CardActions></CardActions>
      </Link>
    </Card>
  );
}
