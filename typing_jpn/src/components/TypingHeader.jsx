import React from "react";
import AppsIcon from "@material-ui/icons/Apps";
import { Tooltip, IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import "../css/Header.css";

export default function TypingHeader(props) {
  return (
    <header>
      <div>
        <h2 className="app-name">
          <Tooltip title="HOME">
            <IconButton aria-label="home">
              <Link to="/missions">
                <AppsIcon />
              </Link>
            </IconButton>
          </Tooltip>
          タイピング
        </h2>
        <h2 className="lesson-title">
          {props.title}
        </h2>
        <h3 className="username">Teakyung</h3>
      </div>
    </header>
  );
}
