// import React, { useState } from "react";
// import Letter from "./Letter";
import { isKanji, toRomaji } from "../LessonData";


// token of a letter
export function Token (letter, sound) {
  this.letter = letter;
  this.sound = sound; // ruby-rt, phonetic
}

export function parseWordsToToken(words) {
  var tokenArray = []

  for (let i = 0; i < words.length; i++) {
    let letter = words[i];
    // var token = new Token(letter, "");

    if (isKanji(letter)) {
      let kanjiBegin = i;
      let kanjiEnd = words.indexOf('(', kanjiBegin);
      let kanji = words.substring(kanjiBegin, kanjiEnd);
      // console.log("kanji: " + kanji + " begin:" + kanjiBegin + " end:" + kanjiEnd);
      
      let soundBegin = kanjiEnd + 1;
      let soundEnd = words.indexOf(')', soundBegin);
      let sound = words.substring(soundBegin, soundEnd);
      // console.log("sound: " + sound + " begin:" + soundBegin + " end:" + soundEnd);

      if (kanjiEnd === -1 || soundEnd === -1) {
        console.log("parsing token missing kanjiEnd:" + kanjiEnd + " soundEnd:" + soundEnd);
        return ""
      }

      if (kanji.length === 1) {
        var token = new Token(kanji, "");
        token.sound = sound;
        tokenArray.push(token);
      } else {
        // let soundArray = sound.match(/(.{1,2})/g);
        let soundArray = sound.split(" ");

        [...kanji].forEach((ji, index) => {
          var token = new Token(ji, "");
          token.sound = soundArray[index];
          tokenArray.push(token);
        })
      }
      i = soundEnd;
    } else {
      let token = new Token(letter, "");
      tokenArray.push(token);
    }
  }

  return tokenArray;
}

// 제목 표시용, 후리가나 빼고 문자열만 리턴
export function getLetterFromToken(words) {
  let tokens = parseWordsToToken(words);
  var letters = "";

  [...tokens].forEach((token) => {
    letters += token.letter;
  });

  return letters;
}
