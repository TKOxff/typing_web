import React, { useState } from "react";
//local
import "../css/Typing.css";

function TypingFeedback(props) {
  return (
    <div className="typing-result">
      <div>
        <img src="/images/typing-speed.svg" alt="speed" />
        <h2>{props.lpm}</h2>
        <h6>LPM</h6>
      </div>
      <div>
        <img src="/images/darts.svg" alt="speed" />
        <h2>{props.accuracy}</h2>
        <h6>%</h6>
      </div>
    </div>
  );
}

export default TypingFeedback;
