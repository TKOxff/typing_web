import React, { useEffect, useState } from "react";
//local
import "../css/Typing.css";
import Words from "./Words";
import TypingInput from "./TypingInput";
import TypingFeedback from "./TypingFeedback";
import GuideKeyboard from "./GuideKeyboard";


export default function TypingNormal(props) {
  console.log(">>>TypingNormal");
  console.log(props);

  function doSubmit(event) {
    event.preventDefault();
  }

  return (
    <div>
      <div className="typing-body">
        <form className={"letters"} onSubmit={doSubmit}>
          <div>
            <Words
              words={props.line}
              letter_count={props.lesson.letter_count}
              typing={props.typing}
              type={props.lesson.type}
              onUpdateAccuracy={props.updateAccuracy}
            />
            <TypingInput doKeyUp={props.doKeyUp} />
          </div>
        </form>
      </div>
      <TypingFeedback accuracy={props.accuracy} lpm={props.lpm} />
      <GuideKeyboard keys={props.currentRomaji} />
    </div>
  );
}
