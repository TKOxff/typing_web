import React, { useState } from "react";
//local
import "../css/Typing.css";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";

const StyledTooltip = withStyles({
  tooltip: {
    fontSize: "1.5rem",
  },
})(Tooltip);

function TypingInput(props) {
  const [isInputFocus, setIsInputFocus] = useState(false);

  return (
    <StyledTooltip
      title="タイピング"
      open={!isInputFocus}
      arrow={true}
      placement="left"
    >
      <input
        type="text"
        onKeyUp={props.doKeyUp}
        name="keyboard"
        autoComplete="off"
        placeholder=""
        autoFocus
        onFocus={() => {
          setIsInputFocus(true);
        }}
        onBlur={() => {
          setIsInputFocus(false);
        }}
      />
    </StyledTooltip>
  );
}

export default TypingInput;
