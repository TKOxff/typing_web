import React, { useEffect, useState } from "react";
//local
import "../css/Typing.css";
import WordsEx from "./WordsEx";
import TypingInput from "./TypingInput";
import TypingFeedback from "./TypingFeedback";
import GuideKeyboard from "./GuideKeyboard";


export default function TypingSentence(props) {
  console.log(">>>TypingSentence");
  console.log(props);

  function doSubmit(event) {
    event.preventDefault();
  }

  return (
    <div>
      <div className="typing-words">
        <form className={"words"} onSubmit={doSubmit}>
          <div>
            <WordsEx
              words={props.words}
              letter_count={props.lesson.letter_count}
              typing={props.typing}
              type={props.lesson.type}
              onUpdateAccuracy={props.updateAccuracy}
            />
            <TypingInput doKeyUp={props.doKeyUp} />
          </div>
        </form>
      </div>
      <TypingFeedback accuracy={props.accuracy} lpm={props.lpm} />
      <GuideKeyboard keys={props.currentRomaji} />
    </div>
  );
}
