import React, { useEffect, useState } from "react";
import GuideKeyboard from "./GuideKeyboard";
import { getLesson, getLessonMaxLv, toRomaji, isAlphabet } from "../LessonData";
//local
import "../css/Typing.css";
import Words from "./Words";
import WordsEx from "./WordsEx";
import { parseWordsToToken } from "./LetterToken";
import TypingFeedback from "./TypingFeedback";
import TypingInput from "./TypingInput";
import TypingFactory from "./TypingFactory";

let keyPressSound = new Audio("/sounds/keypress0.mp3");
const lpmUpdateMs = 500;
const lpmBounsFactor = 1.5; // 실제 분타수 수치가 작은 경향, 조금 늘려준다.

// TypingMain
//   Typing <-
//     TypingFactory
//       TypingNormal
//       TypingSectence
//       TypingTutor
//
export default function Typing(props) {
  // states
  const [step, setStep] = useState(props.lineStep);
  const [typing, setTyping] = useState("");
  const [accuracy, setAccuracy] = useState(100);
  const [lpm, setLPM] = useState(0); // letters per a minute.
  const [isTyping, setIsTyping] = useState(false);
  const [lpmBegin, setLpmBegin] = useState(0);
  const [isInputFocus, setIsInputFocus] = useState(false);

  const lesson = props.lesson;
  const line = lesson.lines[step];
  const maxStep = lesson.lines.length - 1;

  var lineTokens = parseWordsToToken(line);
  // console.log(lineTokens);
  // console.log("typing step:" + step);

  useEffect(() => {
    const inter = setInterval(() => {
      // console.log("===interval call isTyping:" + isTyping);
      if (isTyping === true) {
        let elapsedSec = (Date.now() - lpmBegin) / 1000;
        let scaleInMin = 60 / elapsedSec;

        // console.log("===interval call elapsed:" + Math.floor(elapsedSec) + " scaleInMin:" + scaleInMin);
        let lettersPerMin = Math.floor(
          typing.length * scaleInMin * lpmBounsFactor
        );

        setLPM((old) => {
          return lettersPerMin;
        });
      }
    }, lpmUpdateMs);
    return () => clearInterval(inter);
  });

  function doKeyUp(event) {
    let newValue = event.target.value;
    let lastKey = newValue.charAt(newValue.length - 1);

    let newSound = keyPressSound.cloneNode();
    newSound.volume = 0.2;
    newSound.play();

    if (isTyping === false && newValue.length > 1) {
      setIsTyping(true);
      let timeInMs = Date.now();
      // console.log(timeInMs);
      setLpmBegin(timeInMs);
    }

    if (!isAlphabet(lastKey)) {
      setTyping(newValue);
    }

    let isSkipKey = lastKey.length > 0 ? "[]「」".includes(lastKey) : false;
    // console.log("isSkipKey:" + isSkipKey + " lastKey:" + lastKey);

    if (event.key === "Enter" || isSkipKey) {
      if (newValue.length >= lineTokens.length || isSkipKey) {
        if (step === maxStep) {
          setStep(0);
          props.onNextLevel();
        } else if (lastKey.includes("[「")) {
          setStep((oldStep) => {
            return oldStep - 1;
          });
        } else {
          // console.log("setStep old:" + step);
          setStep((oldStep) => {
            return oldStep + 1;
          });
          // console.log("setStep new:" + step);
          props.onLineClear(step + 1, lpm, accuracy);
        }

        event.target.value = "";
        setTyping("");
        setIsTyping(false);
      }
    }
  }

  function getCursorIndex() {
    let cursor = typing.length / lesson.letter_count;
    cursor = Math.floor(cursor);
    // console.log("cursor:" + cursor + " typing.length:" + typing.length);

    if (cursor >= line.length) {
      return undefined;
    }

    return cursor;
  }

  // 커서에 해당하는 문자수가 1개 이상일 수 있다.
  function getCursorLetters() {
    let cursorIndex = getCursorIndex();

    if (cursorIndex !== undefined) {
      let begin = cursorIndex * lesson.letter_count;
      let end = begin + lesson.letter_count;
      let each = line.substring(begin, end);
      // console.log("each:" + each);
      return each;
    }
    return "";
  }

  function currentRomaji() {
    let cursorIndex = getCursorIndex();
    // console.log("cursorIndex:" + cursorIndex);

    if (cursorIndex === undefined) {
      return "";
    }

    let cursorLetters = getCursorLetters();
    var romaji = toRomaji(cursorLetters);
    if (romaji === undefined) {
      // console.log("undefined cursorLetters:" + cursorLetters);
      return "";
    }
    // console.log("romaji:" + romaji);

    romaji = romaji.toUpperCase().split("").join(" ");
    // console.log("romaji:" + romaji);

    return romaji;
  }

  // function doSubmit(event) {
  //   event.preventDefault();
  // }

  function updateAccuracy(percent) {
    setAccuracy(percent);
  }

  // function isHuriganaWord() {
  //   return lesson.type === "letter" || lesson.type === "tutorial";
  // }

  // 이제 팩토리컴포넌트 하나만 하도록 한다..
  // 하지만, 각 하위컴포넌 별로 사용되지 않는 모든 props를 넘거야 하는 비효율성 (문제 되지 않는다면?)
  return (
    <div>
      <TypingFactory
        factoryId={lesson.type}

        lesson={props.lesson}
        onNextLevel={props.onNextLevel}
        lineStep={props.lineStep}
        onLineClear={props.onLineClear}

        line={line}
        typing={typing}
        doKeyUp={doKeyUp}
        lpm={lpm}
        accuracy={accuracy}
        updateAccuracy={updateAccuracy}
        currentRomaji={currentRomaji()}

        // sentence
        words={lineTokens}
      />
    </div>
  );
}

