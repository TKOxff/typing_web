import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import { Tooltip, IconButton } from "@material-ui/core";
import { Link } from "react-router-dom";
import "../css/Header.css"

function MissionHeader(props) {
  return (
    <header>
      <div>
        <h2 className="app-name">
          <Tooltip title="HOME">
            <IconButton aria-label="home">
              <Link to="/home">
                <HomeIcon />
              </Link>
            </IconButton>
          </Tooltip>
          Missions List
        </h2>
        <h2 className="lesson-title">
          test
        </h2>
        <h3 className="username">Teakyung</h3>
      </div>
    </header>
  );
}

export default MissionHeader;
