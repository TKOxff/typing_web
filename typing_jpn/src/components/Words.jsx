import React, { useState } from "react";
// local
import Letter from "./Letter";
import LetterBox from "./LetterBox";
import { toRomaji } from "../LessonData";


//
// Words is a group of Letters
//
function Words(props) {
  let words = props.words;
  let typings = props.typing;
  // console.log("test:" + toRomaji("か"));

  function eachWordsToArray(src_str, each_count) {
    var j = 0;
    var resultArray = [];
    for (; j < src_str.length; j += each_count) {
      let each = src_str.substring(j, j + each_count);
      // console.log("Words::each " + each);
      resultArray.push(each);
    }
    return resultArray;
  }

  let wordsArray = eachWordsToArray(words, props.letter_count);
  let typingArray = eachWordsToArray(typings, props.letter_count);

  const letters = [];
  const checks = [];
  var checkIndex = 0;
  wordsArray.forEach((c) => {
    letters.push(c);

    if (checkIndex < typingArray.length) {
      if (typingArray[checkIndex] === letters[checkIndex]) {
        checks.push("g");
      } else {
        checks.push("b");
      }
    } else {
      checks.push("n");
    }
    checkIndex++;
  });

  // console.log(checks + " typing:" + typings);

  return (
    <h1>
      {letters.map((letter, id) => {
        return (
          <LetterBox
            key={id}
            sound={toRomaji(letter)}
            letter={letter}
            good={checks[id]}
            type={props.type}
          />
        );
      })}
    </h1>
  );
}

export default Words;
