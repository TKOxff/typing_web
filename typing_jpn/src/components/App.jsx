import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Home";
import TypingMain from "./TypingMain";
import MissionList from "./MissionList";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/main/:mission_id/course/:course_id">
          <TypingMain />
        </Route>

        <Route path="/missions">
          <MissionList />
        </Route>
        
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}
