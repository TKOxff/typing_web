import React, { useState } from "react";
import { Link, Route, Switch, useParams } from "react-router-dom";
import FadeIn from "react-fade-in";
//local
import "../css/App.css";
import TypingHeader from "./TypingHeader";
import Typing from "./Typing";
import { getLesson, getLessonMaxLv, toRomaji } from "../LessonData";
import TypingComplete from "./TypingComplete";
import TypingLineClear from "./TypingLineClear";
// import EmptySpace from "./utils/EmptySpace";
import TypingTutor from "./TypingTutor";
import TypingFactory from "./TypingFactory";

let clearSound = new Audio("/sounds/tone-beep.wav");

// 타자 페이지
export default function TypingMain(props) {
  let { mission_id, course_id } = useParams();
  const missionId = parseInt(mission_id);
  const courseId = parseInt(course_id);

  // states
  const [lessonLv, setLessonLv] = useState(missionId - 1);
  const [isCompleted, setCompleted] = useState(false);
  const [isLineClear, setLineClear] = useState(false);
  const [lineStep, setLineStep] = useState(0);
  const [clearLPM, setClearLPM] = useState(0);
  const [clearAccuray, setClearAccuracy] = useState(0);

  const currentLesson = getLesson(lessonLv, courseId);

  function setNextLessonLv() {
    setCompleted(true);
    setLineStep(0);

    if (lessonLv === getLessonMaxLv(course_id)) {
      setLessonLv(0);
    } else {
      setLessonLv((oldLv) => {
        return oldLv + 1;
      });
    }
  }

  // 한 문장 클리어
  function onLineClear(nextStep, lpm, accuracy) {
    // console.log("onLineClear nextStep:" + nextStep);
    setClearLPM(lpm);
    setClearAccuracy(accuracy);

    setLineClear(true);
    setLineStep(nextStep);

    setTimeout(() => {
      let sound = clearSound.cloneNode();
      sound.volume = 0.5;
      sound.play();
    }, 1000);
  }

  function onLineUserClick(button) {
    // console.log("onLineUserClick");
    setLineClear(false);
  }

  function onUserClick(button) {
    if (button === "Continue") {
      setCompleted(false);
    } else if (button === "Retry") {
      setCompleted(false);
      setLessonLv((oldLv) => {
        return oldLv - 1;
      });
    }
  }

  return (
    <FadeIn>
      <div>
        <TypingHeader title={currentLesson.title} />
        {isCompleted ? (
          <TypingComplete
            mission_id={missionId}
            course_id={courseId}
            onUserClick={onUserClick}
          />
        ) : isLineClear ? (
          <TypingLineClear
            onUserClick={onLineUserClick}
            lpm={clearLPM}
            accuracy={clearAccuray}
          />
        ) : (
          <Typing
            lesson={currentLesson}
            onNextLevel={setNextLessonLv}
            lineStep={lineStep}
            onLineClear={onLineClear}
          />
        )}
      </div>
    </FadeIn>
  );
}
