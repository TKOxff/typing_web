import React, { useEffect, useRef, useState } from "react";
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import * as successAni from "../notties/success.json";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
//local
import "../css/TypingComplete.css";
import useEventListener from "./CustomHooks";
import TypingFeedback from "./TypingFeedback";

const lottieOptions = {
  loop: false,
  autoplay: true,
  animationData: successAni.default,
};

// 타자 페이지
export default function TypingLineClear(props) {
  // TODO: 애니메이션 완료까지는 키 입력을 막아야 한다.
  useEventListener("keydown", (event) => {
    if (event.key === "Enter") {
      props.onUserClick("Continue");
    }
  });

  return (
    <FadeIn>
      <div className="typing-center">
        <TypingFeedback accuracy={props.accuracy} lpm={props.lpm} />
        <Lottie options={lottieOptions} height={256} width={256} /><h2>完了</h2>
        <footer>
          <div className="typing-footer">
            <Button
              variant="outlined"
              color="default"
              onClick={() => {
                props.onUserClick("Retry");
              }}
            >
              Retry
            </Button>
            <span style={{ padding: "0% 10%" }}></span>
            <Tooltip title="ENTER" placement="bottom" arrow="true">
              <Button
                variant="outlined"
                color="default"
                onClick={() => {
                  props.onUserClick("Continue");
                }}
              >
                Continue
              </Button>
            </Tooltip>
          </div>
        </footer>
      </div>
    </FadeIn>
  );
}
