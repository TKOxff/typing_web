import React, { useState } from "react";
import CheckIcon from "@material-ui/icons/Check";
// import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import Zoom from "@material-ui/core/Zoom";
// local
import "../css/Letter.css";

function LetterBox(props) {
  let letter_type = props.type;

  let letterState = letter_type;

  if (props.good === "g") {
    letterState = letter_type + " good";
  } else if (props.good === "b") {
    letterState = letter_type + " bad";
  }

  let checked = props.good === "g";
  // console.log(props.letter + "=" + letterState);

  return (
    <div className="letter-box">
      <div className="letter-elem">
        <Zoom in={checked}>
          <CheckIcon style={{ color: "green", fontSize: 35 }} />
          {/* <RadioButtonUncheckedIcon style={{ color: "green", fontSize: 35 }} /> */}
        </Zoom>
      </div>
      <div className={letterState}>
        <h5 className="sound">{props.sound}</h5>
        <span>{props.letter}</span>
      </div>
    </div>
  );
}

export default LetterBox;
