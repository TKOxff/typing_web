import React, { useState } from "react";
import Keyboard from "react-simple-keyboard";
//local
import "../css/GuideKeyboard.css"

import "react-simple-keyboard/build/css/index.css";
import { isAlphabet } from "../LessonData";

const myLayout = {
  default: [
    "~ 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
    "{tab} Q W E R T Y U I O P { } |",
    '{lock} A S D F G H J K L : " {enter}',
    "{shift} Z X C V B N M < > ? {shift}",
    ".com @ {space} @ @",
  ],
};

function GuideKeyboard(props) {
  // console.log("GuidKeyboard begin");
  // console.log("keys:" + props.keys);

  let buttonKey = isAlphabet(props.keys) ? props.keys : "?";

  const myTheme = {
    buttonTheme: buttonKey !== "?" ? [
      {
        class: "hg-red",
        buttons: buttonKey,
      }
    ] : []
  };

  return (
    <div className="centered">
      <Keyboard
        // onChange={onChange}
        // onKeyPress={onKeyPress}
        layout={myLayout}
        // theme={"hg-theme-default hg-layout-default myTheme"}
        // buttonTheme={myButtonTheme().default}
        buttonTheme={myTheme.buttonTheme}

        display={{
          "{bksp}": "delete",
          "{enter}": "enter",
          "{tab}": "tab",
          "{space}": "space",
          "{shift}": "shift",
          "{lock}": "lock",
        }}
      />
    </div>
  );
}

export default GuideKeyboard;
