import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
// import Button from "@material-ui/core/Button";
import HomeIcon from "@material-ui/icons/Home";
import { Tooltip, IconButton } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import * as legoData from "../notties/loading-cat.json";
// local
import "../css/Home.css";
import MissionCard from "./MissionCard";
import MissionHeader from "./MissionHeader";
import lessonData from "../LessonData";
import { getLetterFromToken } from "./LetterToken";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px",
  },
  paper: {
    padding: theme.spacing(2),
    // textAlign: "center",
    // justify: "center",
    color: theme.palette.text.secondary,
  },
}));

const lottieOptions = {
  loop: true,
  autoplay: true,
  animationData: legoData.default,
};

function MissionList() {
  const thisStyle = useStyles();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, []);

  const courseList = lessonData.map((lesson, courseNo) => (
    <div className="card-grid" key={courseNo}>
      <h2>{lesson.course_title}</h2>
      <Grid
        container
        spacing={4}
        // className = {thisStyle.gridContainer}
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
      >
        {lesson.course_array.map((course, index) => (
          <Grid item xs={12} sm={6} md={4} key={index}>
            <MissionCard
              id={index + 1}
              courseNo={courseNo}
              title={course.title}
              subtitle={
                course.type === "letter"
                  ? course.lines[0]
                  : getLetterFromToken(course.lines[0])
              }
            />
          </Grid>
        ))}
      </Grid>
    </div>
  ));

  return (
    <div>
      {isLoading ? (
        <FadeIn>
          <div className="home-center">
            <Lottie options={lottieOptions} height={120} width={120} />
            <h6>loading...</h6>
          </div>
        </FadeIn>
      ) : (
        <div>
          <FadeIn>
            <MissionHeader />
            <div>{courseList}</div>
          </FadeIn>
        </div>
      )}
    </div>
  );
}

export default MissionList;
