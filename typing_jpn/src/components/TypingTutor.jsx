import React, { useEffect, useState } from "react";
//local
import "../css/Typing.css";
import Words from "./Words";
import TypingInput from "./TypingInput";
import EmptySpace from "./utils/EmptySpace";
import GuideKeyboard from "./GuideKeyboard";


export default function TypingTutor(props) {
  console.log(">>>TypingTutor");
  console.log(props);

  function doSubmit(event) {
    event.preventDefault();
  }

  function updateAccuracy(percent) {
  }

  // Typing_Tutor - Mode - Polymorphism
  return (
    <div>
      <div className="typing-body">
        <form className={"letters"} onSubmit={doSubmit}>
          <div>
            <Words
              words={props.line}
              letter_count={props.lesson.letter_count}
              typing={props.typing}
              type={props.lesson.type}
              onUpdateAccuracy={updateAccuracy}
            />
            <TypingInput doKeyUp={props.doKeyUp} />
          </div>
        </form>
      </div>
      <EmptySpace height="50px" />
      <GuideKeyboard keys={props.currentRomaji} />
    </div>
  );
}
