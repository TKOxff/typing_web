import React, { useState } from "react";
import FadeIn from "react-fade-in";
import Lottie from "react-lottie";
import * as successAni from "../notties/done-trophy.json";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
//local
import "../css/Home.css";
import useEventListener from "./CustomHooks";

const lottieOptions = {
  loop: false,
  autoplay: true,
  animationData: successAni.default,
};

// 타자 페이지
export default function TypingComplete(props) {

  const history = useHistory();

  // console.log(props);
  // TODO: 애니메이션 완료까지는 키 입력을 막아야 한다.
  useEventListener("keydown", (event) => {
    if (event.key === "Enter") {
      // props.onUserClick("Continue");
      history.push("/missions")
    }
  });

  return (
    <FadeIn>
      <div className="typing-center">
        <Lottie options={lottieOptions} height={256} width={256} />
        <h2>Good Job!</h2>
        <footer>
          <div className="typing-footer">
            <Button
              variant="outlined"
              color="default"
              onClick={() => {
                props.onUserClick("Retry");
              }}
            >
              Retry
            </Button>
            <span style={{ padding: "0% 10%" }}></span>
            <Link
              to="/missions"
              style={{ color: "inherit", textDecoration: "inherit" }}
            >
            <Button
              variant="outlined"
              color="default"
              onClick={() => {
                props.onUserClick("Continue");
              }}
            >
              Continue
            </Button>
            </Link>
          </div>
        </footer>
      </div>
    </FadeIn>
  );
}
