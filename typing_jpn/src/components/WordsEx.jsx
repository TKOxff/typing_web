import React, { useState } from "react";
import Letter from "./Letter";

//
// Words is a group of Letters
//
function WordsEx(props) {
  console.log(">>>WordsEx");
  console.log(props);

  let letterTokenArray = props.words;
  let typings = props.typing;

  // console.log(letterTokenArray);
  
  function eachWordsToArray(src_str, each_count) {
    var j = 0;
    var resultArray = [];
    for (; j < src_str.length; j += each_count) {
      let each = src_str.substring(j, j + each_count);
      // console.log("Words::each " + each);
      resultArray.push(each);
    }
    return resultArray;
  }

  // let wordsArray = eachWordsToArray(words, props.letter_count);
  let typingArray = eachWordsToArray(typings, props.letter_count);

  const letters = [];
  const checks = [];
  const sounds = [];
  var checkIndex = 0;

  letterTokenArray.forEach((c) => {
    letters.push(c.letter);
    sounds.push(c.sound);

    if (checkIndex < typingArray.length) {
      if (typingArray[checkIndex] === letters[checkIndex]) {
        checks.push("g");
      } else {
        checks.push("b");
      }
    } else {
      checks.push("n");
    }
    checkIndex++;
  });

  let accuracy = 100;
  if (typings.length > 0) {

    const reducer = (acc, cur) => {
      if (cur === "g") {
        return acc += 1;
      } else {
        return acc += 0;
      }
    };
    let goodCount = checks.reduce(reducer, 0);
    // console.log("good: " + goodCount + " length:" + typings.length);

    accuracy = (goodCount / typings.length) * 100;
    accuracy = Math.round(accuracy);
  }
  // console.log("accuracy: " + accuracy);

  props.onUpdateAccuracy(accuracy);

  // console.log(checks + " typing:" + typings);

  return (
    <h1>
      {letters.map((letter, id) => {
        return (
          <Letter
            key={id}
            sound={sounds[id]}
            letter={letter}
            good={checks[id]}
            type={props.type}
          />
        );
      })}
    </h1>
  );
}

export default WordsEx;
