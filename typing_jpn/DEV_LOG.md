

# TODO

완료한 단계 완료 체크 및 저장!

후리가나 표시 온오프

서버리스 백엔드 React Firebase 조사

사용한 아이콘 이미지 링크 달아주기?
{/* <div>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div> */}

# DONE


# IEDA

한 문장을 완료해서가는 애니메이션 아이콘 표시, 아래 링크의 하트 애니 효과음 처럼
https://www.joshwcomeau.com/react/announcing-use-sound-react-hook/


# ISSUES

문장 연습에서 입력문장과 길이가 정확하게 일치하지 않는 문제
 - 문장에 후리가나 들어가는 한자 사이에 공간이 추가되기 때문이 하나의 이유
 - 후리가나가 없는 문장에도 약간의 오차는 존재한다.


# 참조 사이트

### 小学生の漢字プリント (일본 초등학교 공식)
https://www.kanji1006.com/

### 일본어 자판 입력설명 사이트
https://www.tofugu.com/japanese/how-to-type-in-japanese/

### Kana Converter (후리가나 출력 사이트)
https://nihongodera.com/tools/kana-converter


# Problem Solutions

How to style Material-UI's tooltip?
https://stackoverflow.com/a/54606987/1805215

Add an Image to a placeholder input type
https://stackoverflow.com/a/48819802/12697629

how to simply play an mp3 once onClick in react
https://stackoverflow.com/questions/54114171/how-to-simply-play-an-mp3-once-onclick-in-react

== vscode setting

탐색창 파일 숨기기
files:exclude 대상 파일 추가
https://stackoverflow.com/questions/30140112/how-do-i-hide-certain-files-from-the-sidebar-in-visual-studio-code

React Loading Screen Tactics — Improving User Experience
https://medium.com/front-end-weekly/react-loading-screen-tactics-improving-user-experience-9452f183c00b
