// ViewModel
// The ViewModel is a producer who doesn’t care who consumes data
// is just a regular JavaScript class 
// it can be easily reused anywhere with UI tailored differently.
//
class PokemonViewModel {
  constructor(pokemonStore) {
      this.store = pokemonStore
  }

  getPokemons() {
      return this.store.getPokemons()
  }

  addPokemon(pokemon) {
      this.store.addPokemon(pokemon)
  }

  removePokemon(pokemon) {
      this.store.removePokemon(pokemon)
  }
}

export default PokemonViewModel