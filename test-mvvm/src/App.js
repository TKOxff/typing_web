import { Provider } from 'mobx-react'
import PokemonProvider from './components/PokemonProvider';
import RootStore from './models/RootStore';
import '../css/App.css';

// Level up your React architecture with MVVM
// https://medium.cobeisfresh.com/level-up-your-react-architecture-with-mvvm-a471979e3f21
// 
// MVVM has four main blocks:
//  * The View — UI layer that users interact with,
//  * The ViewController — has access to the ViewModel and handles user input,
//  * The ViewModel — has access to the Model and handles business logic,
//  * The Model — application data source
//

const rootStore = new RootStore()

export default function App() {
  return (
      <Provider {...rootStore.getStores()}>
        <PokemonProvider />
      </Provider>
  );
}
